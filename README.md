# Docker Squid

## Description

Squid config file to provide to [Squid | Ubuntu](https://hub.docker.com/r/ubuntu/squid) for squid to act as an rpm cache for CentOS, ...

## Usage

`cp ~/squid-docker.conf /var/squid.conf`

 then start squid:

`podman run -d --name squid -e TZ=UTC -p 3128:3128 -v /var/squid.conf:/etc/squid/squid.conf ubuntu/squid:5.2-22.04_beta`
